	$(document).ready(function() {
		$("#formFeedback").validate({ 
			debug: false, 
			errorClass: "invalid", 
			validClass: "success", 
			
			rules: { 
				fname: { 
					required: true,
					maxlength: 100,
					alphanumericRus: true
				}, 

				fmail: { 
					required: true,
					email: true,
					maxlength: 50
				}, 

				fpass: {
					required: true,
					maxlength: 10
				}, 
				ftext: {
					required: true,
					maxlength: 500
				} 
			}, 
			messages: { 
				fname: { 
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 100 ��������',
					alphanumericRus: '��������� �����, ����� � �����'
				}, 

				fmail: { 
					required: '����������� ��� ����������',
					email: '������: picom@mail.ru',
					maxlength: '������������ ����� 50 ��������'
				}, 

				fpass: {
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 10 ��������'
				}, 
				ftext: {
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 500 ��������'
				} 
			}
			
		})
	});
