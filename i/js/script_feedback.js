	$(document).ready(function() {
		$("#formFeedback").validate({ 
			debug: false, 
			errorClass: "invalid", 
			validClass: "success", 
			
			rules: { 
				fname: { 
					required: true,
					maxlength: 100,
					alphanumericRus: true
				}, 
				fcomp: {
					maxlength: 100,
					alphanumericRus: true
				}, 
				fcity: {
					maxlength: 100,
					alphanumericRus: true
				}, 
				faddress: { 
					maxlength: 150
				}, 
				fphone: { 
					phoneRus: true,
					maxlength: 50,
					minlength: 5
				}, 
				fmail: { 
					required: true,
					email: true,
					maxlength: 50
				}, 
				fpass: {
					required: true,
					maxlength: 10
				}, 
				ftext: {
					required: true,
					maxlength: 500
				} 
			}, 
			messages: { 
				fname: { 
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 100 ��������',
					alphanumericRus: '��������� �����, ����� � �����'
				}, 
				fcomp: {
					maxlength: '������������ ����� 100 ��������',
					alphanumericRus: '��������� �����, ����� � �����'
				}, 
				fcity: {
					maxlength: '������������ ����� 100 ��������',
					alphanumericRus: '��������� �����, ����� � �����'
				}, 
				faddress: { 
					maxlength: '������������ ����� 150 ��������'
				}, 
				fphone: { 
					phoneRus: '������: +7 (3412) 636 737', 
					maxlength: '������������ ����� 50 ��������',
					minlength: '����������� ����� 5 ��������'
				}, 
				fmail: { 
					required: '����������� ��� ����������',
					email: '������: picom@mail.ru',
					maxlength: '������������ ����� 50 ��������'
				}, 
				fpass: {
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 10 ��������'
				}, 
				ftext: {
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 500 ��������'
				} 
			}
			
		})
	});
