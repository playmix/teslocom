<?php 
	define('ROOT_DIR', dirname (__FILE__));
	define('INC_DIR', ROOT_DIR . '/inc');
	define('LOG_DIR', ROOT_DIR . '/logs');
	define('TMP_DIR', ROOT_DIR . '/tmp');

	require_once('config.php');
	//include('htmlMimeMail.php');

	$mail_file = TMP_DIR . '/mail.tmp';

	if (file_exists($mail_file))
	{
		$body = file_get_contents($mail_file);
		unlink($mail_file);
	}

	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=windows-1251' . "\r\n";
	$headers .= "From: Teslocom.Ru <$notificationEmail>\r\n";
	$headers .= "Host: $notificationEmail\r\n";
	$headers .= 'Reply-To: ' . "$notificationEmail\r\n";
	$headers .= 'X-Mailer: PHP/' . phpversion();
	mail($notificationEmail, "Teslocom.ru update results", $body, $headers);
