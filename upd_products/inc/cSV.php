<?php    /* ��� ANSI ���� */
    class SV { // SV = Speed Vars

       /* ���� ��������� ������� */


       private static function GetVars($arr, $delim) {
           $ret = Array();
           $ret_tmp = explode ($delim, $arr);
           if (is_array($ret_tmp)) foreach ($ret_tmp as $value) if (trim($value)!=='') $ret[]=trim($value);
           return $ret;
       } // ����� �������

       private static function ApplyAction($var, $actions) {
         if (is_array($actions)) {
           if ( in_array('trim', $actions) )        $var = trim($var);
           if ( in_array('intval', $actions) )      $var = intval($var);
           if ( in_array('floatval', $actions) )    $var = floatval($var);
           if ( in_array('round', $actions) )       $var = round($var);
           if ( in_array('round2', $actions) )      $var = round($var,2);
           if ( in_array('html', $actions) )        $var = htmlspecialchars($var);
           if ( in_array('htmld', $actions) )       $var = htmlspecialchars_decode($var);
           if ( in_array('mq', $actions)) if (get_magic_quotes_gpc()) $var = stripslashes($var);
           if ( in_array('js', $actions) )          $var = str_replace("'", "\'", $var);
           if ( in_array('cp1251utf8', $actions) )  $var = iconv('windows-1251','utf-8',$var);
           if ( in_array('md5', $actions) )         $var = md5($var);
           if ( in_array('b64e', $actions) )        $var = base64_encode($var);
           if ( in_array('b64de', $actions) )       $var = base64_decode($var);
           if ( in_array('pgsql', $actions))        $var = pg_escape_string($var);
           if ( in_array('mysql', $actions))        $var = mysql_real_escape_string($var);
           if ( in_array('qt', $actions))           $var = "'".$var."'";
           if ( in_array('qtin', $actions)) {
                $var = explode(',',$var);
                foreach ($var as $key=>$val) $var[$key]= "'".$val."'";
                $var = implode(',', $var);
           }
         }
         return $var;
       } // ����� �������

       private static function GetReqVar($global_name, $keys) {
         $ret = '';
         if (is_array($keys)) {
           $ReqVar = $global_name;
           foreach ($keys as $var) $ReqVar .= "['".$var."']";
           eval('if (isset('.$ReqVar.')) $ret ='.$ReqVar.';');
         }
         return $ret;
       } // ����� �������

       /* ���� ��������� ������� */

       public static function Request($params) {
            $ret = false;
            if ( ereg('\[(.+)\](.*)', $params, $args) )
              if (isset($args[1])) if ($keys = self::GetVars($args[1], ',')) {
                  $ReqVar = self::GetReqVar('$_REQUEST', $keys);
                  if (isset($args[2]))
                    if ( $actions = self::GetVars($args[2], ':') )
                      $ReqVar = self::ApplyAction($ReqVar, $actions);
                  $ret = $ReqVar;
              }
            return $ret;
       } // ����� �������

       public static function Post($params) {
            $ret = false;
            if ( ereg('\[(.+)\](.*)', $params, $args) )
              if (isset($args[1])) if ($keys = self::GetVars($args[1], ',')) {
                  $ReqVar = self::GetReqVar('$_POST', $keys);
                  if (isset($args[2]))
                    if ( $actions = self::GetVars($args[2], ':') )
                      $ReqVar = self::ApplyAction($ReqVar, $actions);
                  $ret = $ReqVar;
              }
            return $ret;
       } // ����� �������

       public static function Get($params) {
            $ret = false;
            if ( ereg('\[(.+)\](.*)', $params, $args) )
              if (isset($args[1])) if ($keys = self::GetVars($args[1], ',')) {
                  $ReqVar = self::GetReqVar('$_GET', $keys);
                  if (isset($args[2]))
                    if ( $actions = self::GetVars($args[2], ':') )
                      $ReqVar = self::ApplyAction($ReqVar, $actions);
                  $ret = $ReqVar;
              }
            return $ret;
       } // ����� �������

       public static function Session($params) {
            $ret = false;
            if ( ereg('\[(.+)\](.*)', $params, $args) )
              if (isset($args[1])) if ($keys = self::GetVars($args[1], ',')) {
                  $ReqVar = self::GetReqVar('$_SESSION', $keys);
                  if (isset($args[2]))
                    if ( $actions = self::GetVars($args[2], ':') )
                      $ReqVar = self::ApplyAction($ReqVar, $actions);
                  $ret = $ReqVar;
              }
            return $ret;
       } // ����� �������

       public static function Globals($params) {
            $ret = false;
            if ( ereg('\[(.+)\](.*)', $params, $args) )
              if (isset($args[1])) if ($keys = self::GetVars($args[1], ',')) {
                  $ReqVar = self::GetReqVar('$GLOBALS', $keys);
                  if (isset($args[2]))
                    if ( $actions = self::GetVars($args[2], ':') )
                      $ReqVar = self::ApplyAction($ReqVar, $actions);
                  $ret = $ReqVar;
              }
            return $ret;
       } // ����� �������


       public static function Variable($var, $params) {
           if ( $actions = self::GetVars($params, ':') )
               $var = self::ApplyAction($var, $actions);
           return $var;
       } // ����� �������

    } // ����� ������

?>