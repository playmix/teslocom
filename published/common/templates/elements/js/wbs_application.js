WbsApplication = newClass(null, {
	contructor: function() {
	},
		
	startLoading: function() {
		if (window.parent && window.parent.showHideLoading)
  		window.parent.showHideLoading(true);
	},
		
	finishLoading: function() {
		if (window.parent && window.parent.showHideLoading)
  		window.parent.showHideLoading(false);
	},
	
	openSubframe: function(url, oldUrl) {
  	this.closeSubframe();
  	
  	if (oldUrl)
  		url = this.getOldUrl(url);
  	
  	var iframe = createElem("iframe", "subframe");
  	iframe.style.width = "100%";
  	iframe.style.height = "100%";
  	iframe.frameBorder = "no";
  	iframe.setAttribute("SCROLLING", "NO");
  	this.startLoading();
  	addHandler(iframe, "load", this.finishLoading, this);
  	//iframe.onload = function() {alert("HELLO"); }
  	
  	var contentBlock = document.getElementById("screen-content-block");
  	contentBlock.insertBefore (iframe, contentBlock.firstChild);
  	
  	iframe.src = url;
  	this.subFrame = iframe;
  },
  
  closeSubframe: function() {
  	if (this.subFrame) {
  		this.subFrame.parentNode.removeChild(this.subFrame);
  		this.subFrame = null;
  	}
  },
  
  getOldUrl: function(url) {
  	return "../html/" + url;
  }
});