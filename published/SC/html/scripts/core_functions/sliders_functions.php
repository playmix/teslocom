<?php
/**
 * Main function for Action 
 */

define('DIR_SLIDERS', DIR_PUBLICDATA_SC.'/sliders');
define('URL_SLIDERS', URL_PRODUCTS_PICTURES.'/../sliders');

   /**
   * Check if isset action
   * @param $aid
   */  
  function SLDRissetAction($aid) {
    if (!empty($aid)){
      $q = db_query("SELECT * FROM SC_actions WHERE id={$aid}");
      $action = db_fetch_row($q);
      if (!empty($action))
        return $action;
    }
    return false;
  }
  
  /**
   * Get action
   * @param $aid
   */  
  function SLDRgetAction($aid) {
    if (!empty($aid)){
      return SLDRissetAction($aid);
    }
    return false;
  }  


  /**
   * Get actions
   */  
  function SLDRgetActions() {
    $actions = array();
    $q = db_query("SELECT * FROM SC_actions");
    while ($r = db_fetch_row($q)){
      $actions[] = $r;
    }    
    return $actions;
  } 
  
  /**
   * Get active action
   */
  function SLDRgetActionsActive() {
    $actions = array();
    $now = date('Y/m/d G:i');
    
    $q = db_query("SELECT * FROM SC_actions WHERE active = 1 AND date_start <= '{$now}' AND date_end >= '{$now}' AND num_pr_rest > 0");
    while ($r = db_fetch_row($q)){
      $actions[] = $r;
    }    
    return $actions;
  }
    
  /**
   * Update exsist action
   * @param $aid
   * @param $data
   */
  function SLDRupdateAction($aid, $data) {
    
    $priorities = scanArrayKeysForID($data, array( "priority" ) );
    $res = SLDRupdateImagePriority($aid, $priorities);
      if(PEAR::isError($res)) break;
    
    if (isset($data['delete_field'])){
      if (!empty($data['delete_field'])){
        foreach ($data['delete_field'] as $id){
          SLDRremoveImage($id);
        }
      }
    }
    
    $active = 0;
    if (isset($data['active']))
      $active = 1;
      
    db_phquery('UPDATE SC_actions SET name = ?, date_start = ?, date_end = ?, product_code = ?, num_pr_all = ?, num_pr_rest = ?, active = ? WHERE id=?', 
      $data['name'], $data['date_start'], $data['date_end'], $data['product_code'], $data['num_pr_all'], $data['num_pr_rest'], $active, $aid );
    
    return true;  
  }

  /**
   * Add new action
   * @param $data
   */
  function SLDRnewAction($data) {
    $active = 0;
    if (isset($data['active']))
      $active = 1;
    
    db_phquery('INSERT INTO SC_actions (name, date_start, date_end, product_code, num_pr_all, num_pr_rest, active) VALUES (?,?,?,?,?,?,?)',
      $data['name'], $data['date_start'], $data['date_end'], $data['product_code'], $data['num_pr_all'], $data['num_pr_rest'], $active);
    
    return db_insert_id();
  }  
  
  /**
   * Remove action
   * @param $aid
   */
  function SLDRremoveAction($aid) {
    SLDRremoveImages($aid);
    db_query("DELETE FROM SC_actions WHERE id=".$aid );
    db_query("DELETE FROM SC_actions_images WHERE aid=".$aid );
  } 
  
  /**
   * Dec num rest of product
   * @param $productID
   * @param $quality
   */
  function SLDRdecNumRestProduct($productID, $quality = 0) {
    if (!empty($productID)){
      $q = db_query("SELECT * FROM SC_products WHERE productID={$productID}");
      $product = db_fetch_row($q);
      
      if (!empty($product))
        db_phquery('UPDATE SC_actions SET num_pr_rest = num_pr_rest - ? WHERE product_code=?', 
          intval($quality), $product['product_code'] );
    }
  }
  
  /**
   * Check data action
   * @param $data
   */
  function SLDRcheckDataAction($data) {
    $errors = array();
    $fields = array('name', 'date_start', 'date_end', 'product_code', 'num_pr_all');
    
    foreach ($fields as $field) {
      if (empty($data[$field])) {
        $errors[] = $field;
      }
    }
    
    if (!empty($errors))
      return $errors;
    
    return false;
  }
  
  /**
   * Get Product
   * @param $code
   */
  function SLDRgetProduct($code) {
    if (!empty($code)) {
      $q = db_query("SELECT * FROM SC_products WHERE product_code='{$code}'");
      $product = db_fetch_row($q);
      if (!empty($product))
        return $product;
    }
    return false;
  } 

  /**
   * Check if isset mailing
   * @param $mid
   */  
  function SLDRissetMailing($mid) {
    if (!empty($mid)){
      $q = db_query("SELECT * FROM SC_actions_mailing WHERE id={$mid}");
      $mailing = db_fetch_row($q);
      if (!empty($mailing))
        return $mailing;
    }
    return false;
  }
  
  /**
   * Get mailing
   * @param $mid
   */  
  function SLDRgetMailing($mid) {
    return SLDRissetMailing($mid);
  }  
  
  /**
   * Get mailings
   */  
  function SLDRgetMailings() {
    $mailings = array();
    $q = db_query("SELECT * FROM SC_actions_mailing");
    while ($r = db_fetch_row($q)){
      $mailings[] = $r;
    }    
    return $mailings;
  } 
    
  /**
   * Update exsist mailing
   * @param $mid
   * @param $data
   */
  function SLDRupdateMailing($mid, $data) {
    db_phquery('UPDATE SC_actions_mailing SET title = ?, text_ru = ? WHERE id=?', 
      $data['title'], $data['text_ru'], $mid );
    
    return true;  
  }

  /**
   * Add new mailing
   * @param $data
   */
  function SLDRnewMailing($data) {  
    db_phquery('INSERT INTO SC_actions_mailing (title, text_ru) VALUES (?,?)',
      $data['title'], $data['text_ru']);
    
    return db_insert_id();
  }  
  
  /**
   * Remove mailing
   * @param $id
   */
  function SLDRremoveMailing($id) {
    db_query("DELETE FROM SC_actions_mailing WHERE id=".$id );
  } 
    
  /**
   * Check data mailing
   * @param $data
   */
  function SLDRcheckDataMailing($data) {
    $errors = array();
    $fields = array('title', 'text_ru');
    
    foreach ($fields as $field) {
      if (empty($data[$field])) {
        $errors[] = $field;
      }
    }
    
    if (!empty($errors))
      return $errors;
    
    return false;
  }  

  /**
   * Get all emails
   */
  function SLDRgetEmails() {
    $mails = array();
    $q = db_query("SELECT * FROM SC_customers");
    while ($r = db_fetch_row($q)){
      $mails[] = $r['Email'];
    }    
    return $mails;    
  }
    
  /**
   * Add image to action
   * @param $aid
   */
  function SLDRaddImage($aid, $standard_file_name, $thumbnail_file_name, $enlarged_file_name, $upload_image_priority){

    if (SLDRissetAction($aid)){
      db_phquery(" INSERT INTO SC_actions_images (aid, filename, thumbnail, enlarged, priority) VALUES( ?, ?, ?, ?,?)", 
        $aid, $standard_file_name, $thumbnail_file_name, $enlarged_file_name, $upload_image_priority);
              
      return db_insert_id();
    }    
    return false;
  }

  /**
   * Add home image
   */
  function SLDRaddHomeImage($standard_file_name, $thumbnail_file_name, $enlarged_file_name, $upload_image_priority){

    db_phquery(" INSERT INTO SC_home_images (filename, thumbnail, enlarged, priority) VALUES( ?, ?, ?,?)", 
      $standard_file_name, $thumbnail_file_name, $enlarged_file_name, $upload_image_priority);
            
    return db_insert_id();
  }
    
  /**
   * Get images of action
   * @param $aid
   */
  function SLDRgetImages($aid){
    if (!empty($aid)){
      $images = array();
      $q = db_query("SELECT * FROM SC_actions_images WHERE aid=".$aid." ORDER BY priority");
      while ($r = db_fetch_row($q)){
        $images[] = $r;
      }    
      return $images;
    }
    return false;
  }  

  /**
   * Get images for home page
   */
  function SLDRgetHomeImages(){
    $images = array();
    $q = db_query("SELECT * FROM SC_home_images ORDER BY priority");
    while ($r = db_fetch_row($q)){
      $images[] = $r;
    }    
    return $images;
  } 
    
  /**
   * Remove image
   * @param $id
   */
  function SLDRremoveImage($id){
    $q=db_query("SELECT filename, thumbnail, enlarged, aid FROM SC_actions_images WHERE id=".$id );
    if ( $image=db_fetch_row($q) )
    {
      if ( $image["filename"]!="" && $image["filename"]!=null )
        if ( file_exists(DIR_SLIDERS."/".$image["filename"]) )
          Functions::exec('file_remove', array(DIR_SLIDERS."/".$image["filename"]));
  
      if ( $image["thumbnail"]!="" && $image["thumbnail"]!=null )
        if ( file_exists(DIR_SLIDERS."/".$image["thumbnail"]) )
          Functions::exec('file_remove', array(DIR_SLIDERS."/".$image["thumbnail"]));
  
      if ( $image["enlarged"]!="" && $image["enlarged"]!=null )
        if ( file_exists(DIR_SLIDERS."/".$image["enlarged"]) )
          Functions::exec('file_remove', array(DIR_SLIDERS."/".$image["enlarged"]));

      db_query("DELETE FROM SC_actions_images WHERE id=".$id );
    }
  }

  /**
   * Remove home image
   * @param $id
   */
  function SLDRremoveHomeImage($id){
    $q=db_query("SELECT filename, thumbnail, enlarged FROM SC_home_images WHERE id=".$id );
    if ( $image=db_fetch_row($q) )
    {
      if ( $image["filename"]!="" && $image["filename"]!=null )
        if ( file_exists(DIR_SLIDERS."/".$image["filename"]) )
          Functions::exec('file_remove', array(DIR_SLIDERS."/".$image["filename"]));
  
      if ( $image["thumbnail"]!="" && $image["thumbnail"]!=null )
        if ( file_exists(DIR_SLIDERS."/".$image["thumbnail"]) )
          Functions::exec('file_remove', array(DIR_SLIDERS."/".$image["thumbnail"]));
  
      if ( $image["enlarged"]!="" && $image["enlarged"]!=null )
        if ( file_exists(DIR_SLIDERS."/".$image["enlarged"]) )
          Functions::exec('file_remove', array(DIR_SLIDERS."/".$image["enlarged"]));

      db_query("DELETE FROM SC_home_images WHERE id=".$id );
    }
  }
    
  /**
   * Remove all images for action
   * @param $aid
   */
  function SLDRremoveImages($aid) {
    $images = SLDRgetImages($aid);
    if (!empty($images)){
      foreach ($images as $image){
        SLDRremoveImage($image['id']);
      }
    }
  }
  
  /**
   * Set priority images
   * @param $aid
   * @param $priorities
   */
  function SLDRupdateImagePriority($aid, $priorities){
    foreach ($priorities as $pid => $scan_info){
      $sql = 'UPDATE SC_actions_images SET priority=? WHERE id=? AND aid=?';
      db_phquery($sql, $scan_info['priority'], $pid, $aid);
    }
  }

  /**
   * Set priority home images
   * @param $priorities
   */
  function SLDRupdateHomeImagePriority($priorities){
    foreach ($priorities as $pid => $scan_info){
      $sql = 'UPDATE SC_home_images SET priority=? WHERE id=?';
      db_phquery($sql, $scan_info['priority'], $pid);
    }
  }
    
          
  /**
   * Upload image
   * @param $aid
   */
  function SLDRuploadImage($data, $is_home = 0) {
    $Register = &Register::getInstance();
    $FilesVar = &$Register->get(VAR_FILES);
    $FilesPostVar = &$Register->get(VAR_POST);
    $error = null;
    
    do{

      if( isset($FilesVar['upload_image'])&&
          isset($FilesVar['upload_image']['name'])&&
          strlen($FilesVar['upload_image']['name'])){
            $file_name = $FilesVar['upload_image']['name'];
      }else{
        return;
      }

      if(!is_image($file_name)){
        $error = PEAR::raiseError(translate('prdset_msg_onlyimages'));
        break;
      }
      
      $file_name = xStripSlashesGPC($file_name);
      $file_name = str_replace('#','',urldecode($file_name));

      if(file_exists(DIR_SLIDERS.'/'.$file_name))
        $file_name = getUnicFile(2, preg_replace('@\.([^\.]+)$@', '%s.$1', $file_name), DIR_SLIDERS);

      $orig_file = DIR_TEMP.'/'.getUnicFile(4, '%s', DIR_TEMP);
      if(isset($FilesVar['upload_image'])&&strlen($FilesVar['upload_image']['name'])){
        $res = Functions::exec('file_move_uploaded', array($FilesVar['upload_image']['tmp_name'], $orig_file));
        if(PEAR::isError($res)){
          $error = $res;
          break;
        }
      }

      $temp_file = DIR_TEMP.'/'.getUnicFile(4, '%s', DIR_TEMP);

      //Standard image       
      $standard_file_name = $file_name;

      if(file_exists(DIR_SLIDERS.'/'.$standard_file_name))
      $standard_file_name = getUnicFile(2, preg_replace('@\.([^\.]+)$@', '%s.$1', $file_name), DIR_SLIDERS);

      $res = Functions::exec('img_resize', array($orig_file, CONF_PRDPICT_STANDARD_SIZE, CONF_PRDPICT_STANDARD_SIZE, $temp_file));

      if(PEAR::isError($res)){
        $error = $res;break;
      }

      $res = Functions::exec('file_copy', array($temp_file, DIR_SLIDERS.'/'.$standard_file_name));
      if(PEAR::isError($res)){
        $error = $res;
        Functions::exec('file_remove', array($temp_file));
        Functions::exec('file_remove', array($orig_file));
        break;
      }

      //Thumbnail image       
      $thumbnail_file_name = preg_replace('@\.([^\.]+)$@', '_thm.$1', $file_name);
      if(file_exists(DIR_SLIDERS.'/'.$thumbnail_file_name))
        $thumbnail_file_name = getUnicFile(2, preg_replace('@\.([^\.]+)$@', '%s.$1', $thumbnail_file_name), DIR_SLIDERS);

      $res = Functions::exec('img_resize', array(DIR_SLIDERS.'/'.$standard_file_name, CONF_PRDPICT_THUMBNAIL_SIZE, CONF_PRDPICT_THUMBNAIL_SIZE, $temp_file));

      if(PEAR::isError($res)){
        $error = $res;break;
      }

      $res = Functions::exec('file_copy', array($temp_file, DIR_SLIDERS.'/'.$thumbnail_file_name));
      if(PEAR::isError($res)){

        $error = $res;
        Functions::exec('file_remove', array($temp_file));
        Functions::exec('file_remove', array($orig_file));
        Functions::exec('file_remove', array(DIR_SLIDERS.'/'.$standard_file_name));
        break;
      }

      //Enlarged image       
      $orig_size = getimagesize($orig_file);
      $standard_size = getimagesize(DIR_SLIDERS.'/'.$standard_file_name);

      if($orig_size[0]>$standard_size[0] || $orig_size[1]>$standard_size[1]){

        $enlarged_file_name = preg_replace('@\.([^\.]+)$@', '_enl.$1', $file_name);
        if(file_exists(DIR_SLIDERS.'/'.$enlarged_file_name))
        $enlarged_file_name = getUnicFile(2, preg_replace('@\.([^\.]+)$@', '%s.$1', $enlarged_file_name), DIR_SLIDERS);

        $res = Functions::exec('file_copy', array($orig_file, DIR_SLIDERS.'/'.$enlarged_file_name));
        if(PEAR::isError($res)){

          $error = $res;
          Functions::exec('file_remove', array($temp_file));
          Functions::exec('file_remove', array($orig_file));
          Functions::exec('file_remove', array(DIR_SLIDERS.'/'.$standard_file_name));
          Functions::exec('file_remove', array(DIR_SLIDERS.'/'.$thumbnail_file_name));
          break;
        }
      }else {
        $enlarged_file_name = '';
      }

      if ($is_home) {
        if (!$pid = SLDRaddHomeImage($standard_file_name, $thumbnail_file_name, $enlarged_file_name, $data['upload_image_priority']))
          $error = 'Ошибка при загрузке.'; 
      } else {
        if (!$pid = SLDRaddImage($data['aid'], $standard_file_name, $thumbnail_file_name, $enlarged_file_name, $data['upload_image_priority']))
          $error = 'Ошибка при загрузке.';
      }
      
      Functions::exec('file_remove', array($temp_file));
      Functions::exec('file_remove', array($orig_file));

    } while(0);

    return $error;
  }
     
?>
