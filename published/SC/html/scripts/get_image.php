<?php
	//for IIS
	if(!isset($_SERVER['DOCUMENT_ROOT'])){ if(isset($_SERVER['SCRIPT_FILENAME'])){
	$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF'])));
	}; };
	if(!isset($_SERVER['DOCUMENT_ROOT'])){ if(isset($_SERVER['PATH_TRANSLATED'])){
	$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr(str_replace('\\\\', '\\', $_SERVER['PATH_TRANSLATED']), 0, 0-strlen($_SERVER['PHP_SELF'])));
	}; };
	//end for IIS
define('WBS_INSTALL_PATH',str_replace(array('\\','///','//'),'/','/'.substr( __gi_cleanpath($_SERVER['SCRIPT_FILENAME']."/../../../../../").'/',strlen($_SERVER['DOCUMENT_ROOT']))));
if(!isset($_GET['__furl_path']))$_GET['__furl_path']='index.php';
define('DIR_ROOT', __gi_cleanpath(dirname($_SERVER['SCRIPT_FILENAME'])));

//PHPSESSID
if(!isset($_COOKIE[ini_get('session.name')])&&!strpos(ini_get('session.name'),'_SC')){
	ini_set('session.name',ini_get('session.name').'_SC');
	if(is_dir(ini_get('session.save_path'))){
		ini_set('session.save_path',ini_get('session.save_path').'/SC_session');
		if(!is_dir(ini_get('session.save_path'))){
			@mkdir(ini_get('session.save_path'));
		}
		ini_set('session.gc_maxlifetime',(3600*8));
	}
}
include(DIR_ROOT.'/includes/init.php');
include_once(DIR_ROOT.'/cfg/connect.inc.wa.php');
$debug=false;

if(isset($_GET['dir'])&&isset($_GET['ext'])&&isset($_GET['filename']))
{
	$directory=urldecode($_GET['dir']);
	$ext=urldecode($_GET['ext']);
	$filename=str_replace('../','',urldecode($_GET['filename']));

	$DB_KEY=SystemSettings::get('DB_KEY');
	
	$filePath=(SystemSettings::is_hosted()?'':'/published')."/publicdata/{$DB_KEY}/attachments/SC/{$directory}/{$filename}.{$ext}";
	$fileAltPath=(SystemSettings::is_hosted()?'':'/published')."/publicdata/TRIALDEMORUS/attachments/SC/{$directory}/{$filename}.{$ext}";
	if(in_array($directory,array('products_pictures','images','themes'))
		&&in_array(strtolower($ext),array('jpg','jpeg','jpe','gif','bmp','pcx','css','xml','png','tif'))){

		if(file_exists('../../../../'.$filePath))
		{
			header("location: http://{$_SERVER['HTTP_HOST']}".str_replace('//','/',WBS_INSTALL_PATH."{$filePath}"));
		}else if(file_exists('../../../../'.$fileAltPath))
		{
			header("location: http://{$_SERVER['HTTP_HOST']}".str_replace('//','/',WBS_INSTALL_PATH."{$fileAltPath}"));
		}else{
			header("HTTP/1.0 404 Not Found");
			print "file <b>{$filePath}</b> and <b>{$fileAltPath}</b> doesn't exits<br>";
		}

	}else{
		header("HTTP/1.0 403 Acces restricted");
	}

}

function __gi_cleanpath($path){
	while (strpos($path,'\\')!==false) {
		$path=str_replace('\\','/',$path);
	}
	while (strpos($path,'//')!==false) {
		$path=str_replace('//','/',$path);
	}
	$res = array();
	$paths = explode('/',$path);
	foreach ($paths as $dir){
		if($dir == '..'){
			array_pop($res);
			continue;
		}
		if($dir == '.'){
			continue;
		}
		array_push($res,$dir);
	}
	return implode('/',$res);
}

?>