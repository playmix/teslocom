var inProgress = false;

// AJAX
var _ajax = function(url, params, callback) {
  $.ajax({
    dataType: "json",
    method: "post",
    url: url,
    data: params,
    beforeSend: function() {
      inProgress = true;
    }
  }).done(function() {
    inProgress = false;
  }).always(function( response ) {
    if (response.success) {
      callback(response);
    }
  });
};

// Simple scroll to anchor up or down the page
var scrollToAnchor = function(anch) {
  $("html, body").animate({
    scrollTop: $(anch).offset().top
  }, "slow", function() {
    //window.location.href = anch;
  });
};

// preload list of models
var _preloadSearchModels = function(form, query) {
  var input = $("input#select-model");

  _ajax('/search/', { bid: $('#select-brand').val(), term: query, action: 'searchbrand' }, function(response){
    if (response.models.length) {
      var ttDropdownMenu = form.find('.tt-dropdown-menu').eq(0), ttDataset1 = ttDropdownMenu.find('.tt-dataset-1').eq(0),
        header = '<div class="dropdown-header">Серии и модели: </div>',
        footer = '<div class="dropdown-footer"><a href="#" class="more">Показать все результаты.</a></div>',
        ttSuggestions = $('<span class="tt-suggestions">');

      $(response.models).each(function(i, elem) {
        ttSuggestions.append( $('<div class="tt-suggestion">').html(elem.value));
      });

      ttDataset1.html('');
      ttDataset1.append(header);
      ttDataset1.append(ttSuggestions);
      ttDataset1.append(footer);
      input.focus();
      ttDropdownMenu.show();
    };
  });
};

$( document ).ready(function() {

  // Fancy init
  if ($(".various").size()){
    $(".various").fancybox({
      width   : 'auto',
      height    : 'auto',
      autoSize  : false,
      closeClick  : false,
      openEffect  : 'none',
      closeEffect : 'none',
      closeBtn  : true
    });
  };

  //fade in/out based on scrollTop value
  $(window).scroll(function (e) {
    e.preventDefault();

    if ($(this).scrollTop() > 100) {
      $('#scroller').fadeIn();
    } else {
      $('#scroller').fadeOut();
    };
  });

  $('.scroll-to').live("click", function(e) {
    e.preventDefault();

    var anch = $(this).attr("href");
    scrollToAnchor(anch);
  });


  $('.iframe').live('click', function(e){
    e.preventDefault();

    if ($(this).hasClass('quick_order_button')) {
      var data = $(this).closest('.product-form').serializeArray();

      for ( var i=0; i<data.length; i++)
        if ( data[i].name == 'action' )
          delete data[i];

      var data_str = $.param(data);
      $(this).attr('href','/quick_order?'+data_str)
    };
  }).fancybox({
    width   : 600,
    height    : 400,
    beforeShow : function(){
      if ($(this.element).hasClass('large')) {
        this.width = 1000;
        this.height = 600;
      }

      if ($(this.element).hasClass('small')) {
        this.width = 400;
        this.height = 300;
      }
    },
    type        : 'iframe',
    autoSize    : false,
    closeClick  : false,
    openEffect  : 'none',
    closeEffect : 'none',
    closeBtn    : true
  });

  $(window).scroll(function(){
    if($(window).scrollTop()>50){
      $(".block-bottom").hide('fast', function() {
        $(".wrapper-block-header").addClass("fixed");
      });
    }else{
      $(".block-bottom").show('fast', function() {
        $(".wrapper-block-header").removeClass('fixed');
      });
    };
  });

  // Init typeahead for products search
  var _renderSearchProducts = function(input) {
    input.typeahead({
      highlight: true,
      hint: false
    },{
      displayKey: 'name',
      source: function (query, process) {
        return _ajax('/search/', { term: query, action: 'autocomplete' }, function(response){
          return process(response.products);
        });
      }, templates: {
        header: '<div class="dropdown-header">Продукты: </div>',
        suggestion: Handlebars.compile(
          '<a href="/product/{{id}}/{{slug_sub}}" {{noindex}}>{{name}}</a>'
        ),
        footer: '<div class="dropdown-footer"><a href="#" class="more">Показать все результаты.</a></div>'
      }
    });
  }($('input.autocomplete'));

  // Init typeahead for models search
  var _renderSearchModel = function(input) {
    input.typeahead({
      highlight: true,
      hint: false,
      minLength: 0
    },{
      displayKey: 'value',
      source: function (query, process) {
        return _ajax('/search/', { bid: $('#select-brand').val(), term: query, action: 'searchbrand' }, function(response){
          return process(response.models);
        });
      }, templates: {
        header: '<div class="dropdown-header">Серии и модели: </div>',
        suggestion: Handlebars.compile(
          '{{value}}'
        ),
        footer: '<div class="dropdown-footer"><a href="#" class="more">Показать все результаты.</a></div>'
      }
    });
  }($('input#select-model'));

  var start = 100;

  $('.block-brand-search .tt-suggestions').live('DOMMouseScroll mousewheel', function (e) {
    if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
      if($(this).scrollTop() + $(this).innerHeight() + 200 >= $(this)[0].scrollHeight && !inProgress) {
        var query = $('#select-model').val();
        _ajax('/search/', { bid: $('#select-brand').val(), term: query, action: 'searchbrand', start: start}, function(response){
          $(response.models).each(function(i, elem) {
            $('.block-brand-search .tt-suggestions').append( $('<div class="tt-suggestion">').html(elem.value));
          });

          start += 100;
        });
      }
    }
  });

/*  var chk_scroll = function(e) {
    var elem = $(e.currentTarget);
    if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) {
      console.log("bottom");
    }
  }

  $('.block-brand-search .tt-suggestions').live('scroll', chk_scroll);*/

  // check if selected brand
  if ($('.select-brand').size() > 0) {
    if ($('.select-brand').val() > 0) {
      var form = $('.select-brand').parents('form').eq(0), input = $("#select-model");
      input.removeAttr('disabled');
      _preloadSearchModels(form, input.val());
    };
  };
});

// Select brand
$('.select-brand').live('change', function(e) {
  var form = $(this).parents('form').eq(0), input = $("input#select-model");
  input.removeAttr('disabled');
  _preloadSearchModels(form, input.val());
});

$("input#select-model").live('focus', function(){
  var form = $(this).parents('form').eq(0), ttDropdownMenu = form.find('.tt-dropdown-menu').eq(0);
  ttDropdownMenu.show();
});

$('.fa-caret-down').live('click', function(e){
  $("input#select-model").focus();
});

$('.tt-suggestion').live('click', function(e) {
  $("input#select-model").typeahead('val', $(this).text());
});

// Select item
$("form .more").live('click' , function(e){
  e.preventDefault();
  $(this).parents('form').eq(0).find("[type=submit]").click();
});

// Delivery page
$('.delivery').live('click', function(e) {
  e.preventDefault();

  var delivery = $(this).data('delivery');
  $('#delivery-type').val(delivery);

  $('.delivery').each(function(){
    $(this).removeClass('active');
  })
  $(this).addClass('active');

  $($(this).data('class')).each(function(){
    if ($(this).hasClass(delivery))
      $(this).show();
    else $(this).hide();
  });
});

// Popover
$('.popover-btn').live('click', function(e){
  e.preventDefault();

  if ($($(this).data('popover')).hasClass('opened')){
    $($(this).data('popover')).removeClass('opened');
  } else {
    $('.popover').removeClass('opened');
    $($(this).data('popover')).addClass('opened');
  };
});

if ($('.popover')) {
  $(document).live('mouseup', function(e){
    var container = $(".wrap-popover");
    if (container.has(e.target).length === 0){
        $('.popover').removeClass('opened');
    };
  });
};

// Toggle show content
if ($('[data-show]')) {
  $('[data-show]').live('click', function(e){
    e.preventDefault();

    if ($(this).hasClass('btn-show')){
      $(this).hide();
      $($(this).data('content')).show();
    };
    if ($(this).hasClass('btn-hide')){
      $(this).parents('[data-wrapper]').eq(0).find('.btn-show').eq(0).show();
      $($(this).data('content')).hide();
    };
  });
};

// Partner chat trigger
if ($('#show-partner-chat')){
  $('#show-partner-chat').live('click', function(e){
    e.preventDefault();

    $('#jivo-label').click();
  });
};

// inc quantity
if ($('.add-quantity')){
  $('.add-quantity').live('click', function(e){
    e.preventDefault();

    var wrap = $(this).parent('.quantity-product');
    var val = wrap.find('.digit').val();
    val++;
    wrap.find('.digit').val(val);
  });
};

// Dec quantity
if ($('.dec-quantity')){
  $('.dec-quantity').live('click', function(e){
    e.preventDefault();

    var wrap = $(this).parent('.quantity-product');
    var val = wrap.find('.digit').val();
    val--;
    if (val >= 0)
      wrap.find('.digit').val(val);
  });
};

// Get price link
if ($('.priceList_type')){
  $('.priceList_type').live('click', function(e){
    var t = $(this).val();
    $('.price-link').each(function(){
      var nwp = '';
      if ($(this).data('nwp'))
        nwp = "&nwp="+$(this).data('nwp');

      $(this).attr('href', '/getpricelist/?t='+t+nwp);
    });
  });
};

// Select view type
if ($('[data-view]')) {
  $('[data-view]').live('click', function(e) {
    e.preventDefault();

    $('[data-view]').each(function(){
      $('.wrapper-products').removeClass($(this).data('view'));
      $(this).removeClass('active');
    });

    $.cookie("view", $(this).data('view'), {
      expires: 7,
      path: "/"
    });

    $(this).addClass('active');
    $('.wrapper-products').addClass($(this).data('view'));
  });
};

//Send message if search empty
$('.block-find-empty [type=button]').live('click', function(e) {
  e.preventDefault();
  var text = $('.block-find-empty #find_text').val(), contact = $('.block-find-empty #find_contact').val(),
    error = $('.block-find-empty .error');

  if (text.trim().length == 0) {
    error.html('Укажите какой товар вы ищите.');
    return;
  };
  if (contact.trim().length == 0) {
    error.html('Укажите как с вами связаться.');
    return;
  };

  $.ajax({
    dataType: "json",
    url: "/search/",
    data: { text: text, contact: contact, action: 'search_empty' }
  }).done(function() {

  }).always(function( response ) {
      if (response.do == 'success') {
        $('.block-find-empty').html("<p class='success'>Сообщение успешно отправлено!</p>");
      };
      if (response.do == 'error') {
        $('.block-find-empty').html("<p class='error'>Произошла ошибка!</p>");
      };
  });
});

// add to cart
$('.add2cart_handler').live('click', function(e) {
  e.preventDefault();

  var objForm = getFormByElem(this);
  if (!objForm)
    return true;

  var r_productParam = getElementsByClass('product_option', objForm);

  var query = '';
  for (var i = r_productParam.length - 1; i >= 0; i--) {
    if (!parseInt(r_productParam[i].value))
      continue;

    if (r_productParam[i].name)
      query += '&' + r_productParam[i].name + '='
        + parseInt(r_productParam[i].value);
  };

  var r_productQty = getElementByClass('product_qty', objForm);
  if (r_productQty) {
    r_productQty = parseInt(r_productQty.value);
    if (r_productQty > 1) {
      query += '&product_qty=' + r_productQty;
    };
  };

  var cart = $('.hndl_proceed_checkout');
  var imgtodrag = $(objForm).find(".product-image").eq(0);

  if (imgtodrag.size() > 0) {
    var imgclone = imgtodrag.clone()
      .offset({
        top: imgtodrag.offset().top,
        left: imgtodrag.offset().left
      })
      .css({
        'opacity': '0.5',
        'position': 'absolute',
        'height': 'auto',
        'width': '100px',
        'z-index': '100'
      })
      .appendTo($('body'))
      .animate({
        'top': cart.offset().top + 10,
        'left': cart.offset().left + 10,
      }, 2000, 'easeInOutExpo');

    imgclone.animate({
      'width': 0,
      'height': 0
    }, function () {
      $(this).detach()
    });
  };

  var url = ORIG_LANG_URL
    + set_query('?ukey=cart&view=noframe&action=add_product&'
    + query + '&productID='
    + objForm.getAttribute('rel'), '');

  addToCart(url);

  $(objForm).find('.block-before-buy').addClass('hide-price');
  setTimeout(function(){
    $(objForm).find('.block-before-buy').hide();
    $(objForm).find('.block-after-buy').show();
  }, 200);
  setTimeout(function(){
    $(objForm).find('.block-after-buy').addClass('show-price');
  }, 250);

  return false;

});


