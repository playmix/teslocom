<?php

define('DIR_SLIDERS', DIR_PUBLICDATA_SC.'/sliders');
define('URL_SLIDERS', URL_PRODUCTS_PICTURES.'/../sliders');

class slidersHomeActions extends ActionsController {

  /**
   * ACTION
   * Save action trigger
   */
  function save_action(){
    
    $data = $this->getData();
    
    $priorities = scanArrayKeysForID($data, array( "priority" ) );
    $res = SLDRupdateHomeImagePriority($priorities);
      if (PEAR::isError($res)) break;
      
    if (isset($data['delete_field'])){
      if (!empty($data['delete_field'])){
        foreach ($data['delete_field'] as $id){
          SLDRremoveHomeImage($id);
        }
      }
    }
    
    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders_home', 'msg_information_save');
  }
  
  /**
   * ACTION
   * Ulpoad image.
   */
  function save_images(){
    $data = $this->getData();    
    $error = SLDRuploadImage($data, 1);
    
    if (!empty($error))
      Message::raiseMessageRedirectSQ(MSG_ERROR, 'ukey=sliders_home', $error);
    else
      Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders_home', 'msg_information_save');
  }
  
  /**
   * Main trigger
   */
  function main() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    /* @var $smarty Smarty */
    $GetVars = &$Register->get(VAR_GET);
    
    $smarty->assign('URL_SLIDERS',URL_SLIDERS);
    
    //Product images       
    $images = SLDRgetHomeImages();      
    foreach ($images as $_ind=>$_val){
      if ( file_exists(DIR_SLIDERS.'/'.$images[$_ind]['filename']) && trim($images[$_ind]['filename']) != '' ){
        $images[$_ind]['image_exists'] = 1;
        list($images[$_ind]['image_width'], $images[$_ind]['image_height']) = getimagesize(DIR_SLIDERS.'/'.$images[$_ind]['filename']);
        $images[$_ind]['large_image'] = array('size' => sprintf('%0.0f KB',round(filesize(DIR_SLIDERS.'/'.$images[$_ind]['filename'])/1024)),'file' => $images[$_ind]['filename'], 'width' => $images[$_ind]['image_width'], 'height' => $images[$_ind]['image_height']);
      }else{
        $images[$_ind]['large_image'] = array('size' => 0,'file' => $images[$_ind]['filename'], 'width' => 0, 'height' => 0);
      }
      if ( file_exists(DIR_SLIDERS.'/'.$images[$_ind]['enlarged']) && trim($images[$_ind]['enlarged']) != '' ){
        $images[$_ind]['enlarged_exists'] = 1;
        list($images[$_ind]['enlarged_width'], $images[$_ind]['enlarged_height']) = getimagesize(DIR_SLIDERS.'/'.$images[$_ind]['enlarged']);
        $images[$_ind]['enlarged_image'] = array('size' => sprintf('%0.0f KB',round(filesize(DIR_SLIDERS.'/'.$images[$_ind]['enlarged'])/1024)),'file' => $images[$_ind]['enlarged'], 'width' => $images[$_ind]['enlarged_width'], 'height' => $images[$_ind]['enlarged_height']);
      }elseif($images[$_ind]['image_exists'] == 1){
        $images[$_ind]['enlarged_exists'] = 1;
        $images[$_ind]['enlarged_image'] = $images[$_ind]['large_image'];
      }else{
        $images[$_ind]['enlarged_image'] = array('size' =>0,'file' => $images[$_ind]['enlarged'], 'width' => 0, 'height' => 0);
      }

      if ( file_exists(DIR_SLIDERS.'/'.$images[$_ind]['thumbnail'])&& trim($images[$_ind]['thumbnail']) != '' ){
        $images[$_ind]['thumbnail_exists'] = 1;
        list($images[$_ind]['thumbnail_width'], $images[$_ind]['thumbnail_height']) = getimagesize(DIR_SLIDERS.'/'.$images[$_ind]['thumbnail']);
        $images[$_ind]['thumbnail_image'] = array('size' => sprintf('%0.0f KB',round(filesize(DIR_SLIDERS.'/'.$images[$_ind]['thumbnail'])/1024)),'file' => $images[$_ind]['thumbnail'], 'width' => $images[$_ind]['thumbnail_width'], 'height' => $images[$_ind]['thumbnail_height']);
      }else{
        $images[$_ind]['thumbnail_image'] = array('size' =>0,'file' => $images[$_ind]['thumbnail'], 'width' => 0, 'height' => 0);
      }

    }
    $smarty->assign('images',$images);
    
    $smarty->assign('admin_sub_dpt', 'sliders_home.html');
  }
  
}

ActionsController::exec('slidersHomeActions'); 

?>