<?php

class saveRss2XmlController extends ActionsController {

	private $_template = 'saveRss2Xml.html';
	private $_file_name = 'rss.xml';

	/* ACTION */
	public function save()
	{
		$Register = &Register::getInstance();
		$PostVars = &$Register->get(VAR_POST);
		$smarty = &$Register->get(VAR_SMARTY);
		
		if ( isset($PostVars['text']) )
			$success = $this->_saveFile($PostVars['text']);
		
		$smarty->assign('text', $this->_readFile());
		$smarty->assign('success', $success);
		$smarty->assign('admin_sub_dpt', $this->_template);
	}

	private function _readFile()
	{
		$content = '';
		if ( file_exists($this->_getFileName()) )
			$content = file_get_contents($this->_getFileName());
		
		return $content;
	}
	
	private function _saveFile($content)
	{
		return file_put_contents($this->_getFileName(),$content);
	}
	
	
	private function _getFileName()
	{
		return DIR_DATA_SC.'/'.$this->_file_name;
	}
	
	public function main()
	{
		$Register = &Register::getInstance();
		$smarty = &$Register->get(VAR_SMARTY);
		$smarty->assign('text', $this->_readFile());
		$smarty->assign('admin_sub_dpt', $this->_template);
	}

}

ActionsController::exec('saveRss2XmlController');