<?php
define('CARTVIEW_FADE', 'fade');
define('CARTVIEW_FRAME', 'frame');
define('CARTVIEW_WIDGET', 'widget');

class InflowController extends ActionsController{

  function main(){
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    /*@var $smarty Smarty*/
    // shopping cart

    if ($user = PRTNRget()){
      $GetVars = &$Register->get(VAR_GET);
      $page = (isset($GetVars['offset']) ? intval($GetVars['offset']) : 0);
      $show_all = (isset($GetVars['show_all']) ? 1 : 0);
      NFLWsetLastVisit($user['customerID']);

      $inflows = NFLWgets($page, $show_all);
      $inflow_navigator = NFLWgetNavigator($page, $show_all);

      $smarty->assign('inflows', $inflows);
      $smarty->assign('inflow_navigator', $inflow_navigator);
    }

    $smarty->assign('main_content_template', 'inflow.html');

  }
}

ActionsController::exec('InflowController');
?>