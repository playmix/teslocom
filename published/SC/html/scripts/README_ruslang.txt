-
@features "SS Premium product"
@state begin
-

	+---------------------+
	| Shop-Script PREMIUM |
	+---------------------+

Спасибо за Ваш выбор Shop-Script PREMIUM! 
-
@features "SS Premium product"
@state end
-
-
@features "SS Pro product"
@state begin
-

	+-----------------+
	| Shop-Script PRO |
	+-----------------+

Спасибо за Ваш выбор Shop-Script PRO! 
-
@features "SS Pro product"
@state end
-

Перед использованием продукта, пожалуйста, скачайте
Руководство Пользователя Shop-Script PRO на сайте
http://www.shop-script.ru
Руководство Пользователя содержит подробные инструкции
по установке и использованию продукта.

----
Copyright (c) WebAsyst LLC, 2005. Все права защищены.
http://www.shop-script.ru