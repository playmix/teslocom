<?php
	class htmlCodesManager{
		
		/**
		 * Return assoc array of all html codes
		 *
		 * @return array - ('&lt;code_key&gt;' =&gt; '&lt;code_info&gt;')
		 */
		function getCodesList(){
			
			$codes = array();
			
			$dbq = "SELECT `key`,`title` FROM ".HTMLCODES_TABLE." ORDER BY `title`";
			$dbres = db_query($dbq);
			while ($row = db_fetch_assoc($dbres)){
				
				$codes[] = $row;
			}
			
			return $codes;
		}
		
		static function getCodeInfo($code_key){
			
			$dbq = "SELECT * FROM ".HTMLCODES_TABLE." WHERE `key`=?";
			$dbres = db_phquery($dbq, $code_key);
			$code_info = db_fetch_assoc($dbres);
			return $code_info;
		}
		
		function addCode($params){
			
			$dbq = "INSERT IGNORE ".HTMLCODES_TABLE." (`key`, `title`, `code`) VALUES (?key, ?title, ?code)";
			db_phquery($dbq, $params);
		}
		
		function updateCode($key, $params){
			$params['orig_key'] = $key;
			$dbq = "UPDATE ".HTMLCODES_TABLE." SET `key`=?key, `title`=?title, `code`=?code WHERE `key`=?orig_key";
			$res = db_phquery($dbq, $params);
			if(($res['resource'] === true) &&(db_affected_rows() === 0)){
				$params['key'] = $key;
				self::addCode($params);
			}
		}
	
		function deleteCode($key){
			
			db_phquery("DELETE FROM ?#HTMLCODES_TABLE WHERE `key`=?", $key);
		}
	
		function renderCodeKey(){
			
			$max = 1;
			do{
				$_key = rand_name(8);
				$code_info = htmlCodesManager::getCodeInfo($_key);
			}while (isset($code_info['key']) && 100>$max++);
			
			return $_key;
		}
	}
?>